package com.ynms.yournextmoonshot;

import org.joda.time.DateTime;

public class Utils {

    private static final String DATE_FORMAT = "dd-MM-yyyy HH:mm:ss";

    public static String getFullDateString(DateTime dateTime) {
        return dateTime.toString(DATE_FORMAT);
    }
}
