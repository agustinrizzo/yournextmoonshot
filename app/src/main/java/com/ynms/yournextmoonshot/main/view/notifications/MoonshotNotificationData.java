package com.ynms.yournextmoonshot.main.view.notifications;

public class MoonshotNotificationData {

    private String moonshotName;
    private String weatherIcon;
    private int daysToMoonshot;

    public MoonshotNotificationData(String moonshotName, String weatherIcon, int daysToMoonshot) {
        this.moonshotName = moonshotName;
        this.weatherIcon = weatherIcon;
        this.daysToMoonshot = daysToMoonshot;
    }

    public String getMoonshotName() {
        return moonshotName;
    }

    public void setMoonshotName(String moonshotName) {
        this.moonshotName = moonshotName;
    }

    public String getWeatherIcon() {
        return weatherIcon;
    }

    public void setWeatherIcon(String weatherIcon) {
        this.weatherIcon = weatherIcon;
    }

    public int getDaysToMoonshot() {
        return daysToMoonshot;
    }

    public void setDaysToMoonshot(int daysToMoonshot) {
        this.daysToMoonshot = daysToMoonshot;
    }
}
