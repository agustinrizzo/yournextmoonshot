package com.ynms.yournextmoonshot.main.view;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.ynms.yournextmoonshot.R;
import com.ynms.yournextmoonshot.camera.Fov;
import com.ynms.yournextmoonshot.capture.view.CaptureActivity;
import com.ynms.yournextmoonshot.main.model.Moonshot;
import com.ynms.yournextmoonshot.main.view.countdown.CountdownView;
import com.ynms.yournextmoonshot.main.view.moonshots_listing.MoonshotsAdapter;
import com.ynms.yournextmoonshot.main.view.moonshots_listing.MoonshotsRecyclerView;
import com.ynms.yournextmoonshot.main.view.notifications.MoonshotNotificationData;
import com.ynms.yournextmoonshot.main.viewmodel.MainViewModel;
import com.ynms.yournextmoonshot.model.moon_moments.moon_moment.MoonMoment;
import com.ynms.yournextmoonshot.moonshot.MoonshotResumeDialog;
import com.ynms.yournextmoonshot.moonshot.MoonshotViewPropertiesDialog;
import com.ynms.yournextmoonshot.sensors.Orientation;

import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Notification.EXTRA_NOTIFICATION_ID;


public class MainActivity extends AppCompatActivity {

    private static final String LOGTAG = MainActivity.class.getSimpleName();

    private int REQUEST_CODE_PERMISSIONS = 101;
    private final String[] REQUIRED_PERMISSIONS = new String[]{
            "android.permission.CAMERA",
            "android.permission.WRITE_EXTERNAL_STORAGE"
    };

    @BindView(R.id.countdownView)
    CountdownView countdownView;

    @BindView(R.id.capturesRecyclerView)
    MoonshotsRecyclerView moonshotsRecyclerView;

    @BindView(R.id.textViewEmptyMessage)
    TextView textViewEmptyMessage;

    @BindView(R.id.buttonToTheMoon)
    ExtendedFloatingActionButton buttonToTheMoon;

    private MainViewModel mainViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);

        if (!allPermissionsGranted()){
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }

        initGui();
        initViewModel();
    }

    private void initGui() {
        /*
        WeatherAPI weatherAPI = NetworkClient.getRetrofitClient().create(WeatherAPI.class);
        Call<WeatherResponse> call =
                weatherAPI.getWeatherData(35, 139, "metric", "c4c405983d8b2c82ce8aa1d3b0cacb2f");
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                Toast.makeText(
                        MainActivity.this,
                        Integer.toString(response.body().getList().size()),
                        Toast.LENGTH_LONG
                ).show();
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
            }
        });
        */

        initRecyclerView();
        buttonToTheMoon.setOnClickListener(v -> {
            Intent intent = new Intent(this, CaptureActivity.class);
            startActivityForResult(intent, 1);
        });
        //initC();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    Moonshot moonshot = (Moonshot) bundle.getSerializable("moonshot");
                    mainViewModel.persisteMoonshot(moonshot);
                }
            } else if (resultCode == 0) {
                Log.d(LOGTAG, "RESULT CANCELLED");
            }
        }
    }

    private void initRecyclerView() {
        moonshotsRecyclerView.setOnHolderButtonClickListener(new MoonshotsAdapter.OnHolderButtonClickListener() {
            @Override
            public void onHolderPropertiesButtonClick() {
                initCaptureConfigFragment();
            }

            private void initCaptureConfigFragment() {
                final MoonshotViewPropertiesDialog moonshotViewPropertiesDialog =
                        new MoonshotViewPropertiesDialog();
                moonshotViewPropertiesDialog.setOnCloseListener(
                        () -> moonshotViewPropertiesDialog.dismiss()
                );
                moonshotViewPropertiesDialog
                        .show(getSupportFragmentManager(), "moonshot_view_properties_dialog");
            }

            @Override
            public void onHolderLocationButtonClick(Intent startActivityIntent) {
                startActivity(startActivityIntent);
            }

            @Override
            public void onHolderShareButtonClick(Intent sendIntent) {
                startActivity(Intent.createChooser(sendIntent, "Compartir con..."));
            }
        });
    }

    /*
    private void initC() {
        LocalDateTime localDateTime = new LocalDateTime(2019,10, 20, 0, 0, 0);
        long diffInMs = localDateTime.toDateTime().getMillis() - System.currentTimeMillis();
        countdownView.start(diffInMs);
    }
    */

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_CODE_PERMISSIONS){
            if (!allPermissionsGranted()){
                Toast.makeText(this, "Permissions not granted by the user.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private boolean allPermissionsGranted(){
        for (String permission : REQUIRED_PERMISSIONS){
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED){
                return false;
            }
        }
        return true;
    }

    private void initViewModel() {
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        mainViewModel.getMoonshotsLiveData().observe(this, moonshots -> {
            if (moonshots.size() > 0) {
                moonshotsRecyclerView.setVisibility(View.VISIBLE);
                textViewEmptyMessage.setVisibility(View.GONE);
                moonshotsRecyclerView.submitCaptures(moonshots);
            } else {
                moonshotsRecyclerView.setVisibility(View.GONE);
                textViewEmptyMessage.setVisibility(View.VISIBLE);
            }
        });

        mainViewModel.getMoonshotProcessedEvent().observe(this, aBoolean -> {
            final MoonshotResumeDialog moonshotResumeDialog = new MoonshotResumeDialog();
            moonshotResumeDialog.setOnCloseListener(
                    () -> moonshotResumeDialog.dismiss()
            );
            moonshotResumeDialog.show(getSupportFragmentManager(), "moonshot_resume_dialog");
        });

        /*
        Moonshot testMoonshot = new Moonshot(
                "Moon Party!!!", //null,
                -3.0f, -4.0f, new Orientation(1.0f, 2.0f),
                0.88f, 0.7f, 1, "/storage/emulated/0/Pictures/capture1571351836085.png",
                new DateTime(),
                new Fov(5.0f, 6.0f));
        mainViewModel.persisteMoonshot(testMoonshot);
        */
    }
}
