package com.ynms.yournextmoonshot.main.model;

import android.location.Location;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.ynms.yournextmoonshot.camera.Fov;
import com.ynms.yournextmoonshot.sensors.Orientation;

import org.joda.time.DateTime;

import java.io.Serializable;

@Entity(tableName = "moonshot")
public class Moonshot implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private long id;

    @ColumnInfo(name = "name")
    private String name;

    /*
    @ColumnInfo(name = "location")
    private Location location;
    */
    @ColumnInfo(name = "longitude")
    private Float longitude;
    @ColumnInfo(name = "latitude")
    private Float latitude;
    @ColumnInfo(name = "orientation")
    private Orientation orientation;

    @ColumnInfo(name = "minMoonIllumination")
    private Float minMoonIllumination;
    @ColumnInfo(name = "minSkyDarkness")
    private Float minSkyDarkness;
    @ColumnInfo(name = "lens")
    private Integer lens;
    @ColumnInfo(name = "capturePath")
    private String capturePath;

    @ColumnInfo(name = "captureDate")
    private DateTime captureDate;

    @ColumnInfo(name = "fov")
    private Fov fov;

    @ColumnInfo(name = "processing")
    private Boolean processing;
    @ColumnInfo(name = "targetDate")
    private DateTime targetDate;


    public Moonshot() {
    }

    public Moonshot(String name,
                    Float longitude, Float latitude, Orientation orientation,
                    Float minMoonIllumination, Float minSkyDarkness, Integer lens, String capturePath,
                    DateTime captureDate,
                    Fov fov) {
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.orientation = orientation;
        this.minMoonIllumination = minMoonIllumination;
        this.minSkyDarkness = minSkyDarkness;
        this.lens = lens;
        this.capturePath = capturePath;
        this.captureDate = captureDate;
        this.fov = fov;
        processing = true;
        targetDate = new DateTime(0);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public Float getMinMoonIllumination() {
        return minMoonIllumination;
    }

    public void setMinMoonIllumination(Float minMoonIllumination) {
        this.minMoonIllumination = minMoonIllumination;
    }

    public Float getMinSkyDarkness() {
        return minSkyDarkness;
    }

    public void setMinSkyDarkness(Float minSkyDarkness) {
        this.minSkyDarkness = minSkyDarkness;
    }

    public Integer getLens() {
        return lens;
    }

    public void setLens(Integer lens) {
        this.lens = lens;
    }

    public String getCapturePath() {
        return capturePath;
    }

    public void setCapturePath(String capturePath) {
        this.capturePath = capturePath;
    }

    public DateTime getCaptureDate() {
        return captureDate;
    }

    public void setCaptureDate(DateTime captureDate) {
        this.captureDate = captureDate;
    }

    public Fov getFov() {
        return fov;
    }

    public void setFov(Fov fov) {
        this.fov = fov;
    }

    public Boolean getProcessing() {
        return processing;
    }

    public void setProcessing(Boolean processing) {
        this.processing = processing;
    }

    public DateTime getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(DateTime targetDate) {
        this.targetDate = targetDate;
    }
}
