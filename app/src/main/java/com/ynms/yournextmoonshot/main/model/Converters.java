package com.ynms.yournextmoonshot.main.model;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.ynms.yournextmoonshot.camera.Fov;
import com.ynms.yournextmoonshot.sensors.Orientation;

import org.joda.time.DateTime;

import java.util.ArrayList;

public class Converters {

    @TypeConverter
    public static DateTime toDateTime(Long milliseconds) {
        return new DateTime();
    }

    @TypeConverter
    public Long fromDateTime(DateTime dateTime) {
        return dateTime.getMillis();
    }

    @TypeConverter
    public static Orientation toOrientation(String json) {
        return new Gson().fromJson(json, Orientation.class);
    }

    @TypeConverter
    public static String fromOrientation(Orientation orientation) {
        return new Gson().toJson(orientation);
    }

    @TypeConverter
    public static Fov toFov(String json) {
        return new Gson().fromJson(json, Fov.class);
    }

    @TypeConverter
    public static String fromFov(Fov fov) {
        return new Gson().toJson(fov);
    }
}
