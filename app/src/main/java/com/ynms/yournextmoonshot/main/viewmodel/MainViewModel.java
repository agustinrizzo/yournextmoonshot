package com.ynms.yournextmoonshot.main.viewmodel;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ynms.yournextmoonshot.R;
import com.ynms.yournextmoonshot.main.model.Moonshot;
import com.ynms.yournextmoonshot.main.model.MoonshotDao;
import com.ynms.yournextmoonshot.main.view.MainActivity;
import com.ynms.yournextmoonshot.main.view.notifications.MoonshotNotificationData;
import com.ynms.yournextmoonshot.model.database.AppDatabase;
import com.ynms.yournextmoonshot.model.moon_moments.MoonMomentsRepository;
import com.ynms.yournextmoonshot.model.moon_moments.moon_moment.MoonMoment;

import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.List;

import static android.app.Notification.EXTRA_NOTIFICATION_ID;


public class MainViewModel extends AndroidViewModel {

    private static final String CHANNEL_ID = "MOONSHOTS";

    // Moonshots database
    private MoonshotDao moonshotDao;

    //
    private MoonMomentsRepository moonMomentsRepository;

    private long id;

    private LiveData<List<Moonshot>> moonshotsLiveData;
    public LiveData<List<Moonshot>> getMoonshotsLiveData() {
        return moonshotsLiveData;
    }

    private MutableLiveData<Boolean> moonshotProcessedEvent = new MutableLiveData<>();
    public LiveData<Boolean> getMoonshotProcessedEvent() {
        return moonshotProcessedEvent;
    }

    public MainViewModel(@NonNull Application application) {
        super(application);

        moonMomentsRepository = new MoonMomentsRepository(getApplication());

        moonshotDao = AppDatabase.getAppDatabase(getApplication()).moonshotDao();
        moonshotDao.nukeTable();

        moonshotsLiveData = moonshotDao.getAllMoonshotsLiveData();

        createNotificationChannel();
        //TestNotificationThread testNotificationThread = new TestNotificationThread();
        //testNotificationThread.start();
    }

    public void persisteMoonshot(Moonshot moonshot) {
        id = moonshotDao.insertMoonshot(moonshot);

        ProcessingThread processingThread = new ProcessingThread();
        processingThread.start();
    }

    class ProcessingThread extends Thread {
        public void run() {
            try {
                Thread.sleep(5000);
                Moonshot insertedMoonshot = moonshotDao.findMoonshotById(id);
                insertedMoonshot.setProcessing(false);
                DateTime targetDate = new DateTime().plusDays(4).plusHours(7).plusMinutes(4);
                insertedMoonshot.setTargetDate(targetDate);
                moonshotDao.updateMoonshot(insertedMoonshot);
                moonshotProcessedEvent.postValue(true);
            } catch (Exception e) {
            }
        }
    }

    public void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getApplication().getString(R.string.channel_name);
            String description = getApplication().getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getApplication().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public void buildNotification(MoonshotNotificationData moonshotNotificationData) {
        HashMap<String, Integer> weatherIconNameDrawable = new HashMap<>();
        //clear sky
        weatherIconNameDrawable.put("01d", R.drawable.w01d);
        weatherIconNameDrawable.put("01n", R.drawable.w01n);
        //few clouds
        weatherIconNameDrawable.put("02d", R.drawable.w02d);
        weatherIconNameDrawable.put("02n", R.drawable.w02n);
        //scattered clouds
        weatherIconNameDrawable.put("03d", R.drawable.w03d);
        weatherIconNameDrawable.put("03n", R.drawable.w03n);
        //broken clouds
        weatherIconNameDrawable.put("04d", R.drawable.w04d);
        weatherIconNameDrawable.put("04n", R.drawable.w04n);
        //shower rain
        weatherIconNameDrawable.put("09d", R.drawable.w09d);
        weatherIconNameDrawable.put("09n", R.drawable.w09n);
        //rain
        weatherIconNameDrawable.put("10d", R.drawable.w10d);
        weatherIconNameDrawable.put("10n", R.drawable.w10n);
        //thunderstorm
        weatherIconNameDrawable.put("11d", R.drawable.w11d);
        weatherIconNameDrawable.put("11n", R.drawable.w11n);
        //snow
        weatherIconNameDrawable.put("13d", R.drawable.w13d);
        weatherIconNameDrawable.put("13n", R.drawable.w13n);
        //mist
        weatherIconNameDrawable.put("50d", R.drawable.w50d);
        weatherIconNameDrawable.put("50n", R.drawable.w50n);

        Bitmap icon = BitmapFactory.decodeResource(
                getApplication().getResources(),
                weatherIconNameDrawable.get(moonshotNotificationData.getWeatherIcon())
        );

        Intent snoozeIntent = new Intent(getApplication(), MainActivity.class);
        snoozeIntent.setAction("ACTION_SNOOZE");
        snoozeIntent.putExtra(EXTRA_NOTIFICATION_ID, 0);
        PendingIntent snoozePendingIntent =
                PendingIntent.getBroadcast(getApplication(), 0, snoozeIntent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplication(), CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_brightness_3_grey_700_36dp)
                .setContentTitle(String.format(
                        "Hoy es el dia!!!" /*"Faltan %d días para tu moonshot",
                        moonshotNotificationData.getDaysToMoonshot()/*,
                        moonshotNotificationData.getMoonshotName()*/))
                .setContentText("El clima es perfecto para hacer historia :D" /*alertNotificationData.getSubtitleValue()*/)
                .setLargeIcon(icon)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                //.setContentIntent(getAlertActivityPendingIntent(alertNotificationData))
                .setAutoCancel(true);
                /*
                .addAction(
                        R.drawable.ic_brightness_3_grey_700_36dp,
                        "RECALCULAR MOONSHOT",
                        snoozePendingIntent
                );
                */

        // Gets an instance of the NotificationManager service
        NotificationManager notificationManager =
                (NotificationManager) getApplication().getSystemService(Context.NOTIFICATION_SERVICE);
        // to post your notification to the notification bar with a id. If a notification with same
        // id already exists, it will get replaced with updated information.
        if (notificationManager != null) {
            notificationManager.notify(1/*alertNotificationData.getIdValue()*/, builder.build());
        }
    }

    class TestNotificationThread extends Thread {
        public void run() {
            try {
                Thread.sleep(5000);
                buildNotification(new MoonshotNotificationData(
                        "My Perfect Moonshot", "01d", 0));
            } catch (Exception e) {
            }
        }
    }

    private class ComputeMoonshotAsyncTask extends AsyncTask<Void, Integer, DateTime> {

        private Moonshot moonshot;

        ComputeMoonshotAsyncTask(Moonshot moonshot) {
            this.moonshot = moonshot;
        }

        @Override
        protected DateTime doInBackground(Void... params) {
            float captureAzimuth = moonshot.getOrientation().getAzimuth();
            float lowerAzimuth = captureAzimuth - (moonshot.getFov().getHorizonalAngle() / 2);
            float higherAzimuth = captureAzimuth + (moonshot.getFov().getHorizonalAngle() / 2);

            float captureElevation = moonshot.getOrientation().getElevation();
            float lowerElevation = captureElevation - (moonshot.getFov().getVerticalAngle() / 2);
            if (lowerElevation < 0) {
                lowerElevation = 0;
            }
            float higherElevation = captureElevation + (moonshot.getFov().getVerticalAngle() / 2);

            List<MoonMoment> moonMoments = moonMomentsRepository.getRestrictedMoonMoments(
                    moonshot.getCaptureDate().getMillis(),
                    lowerAzimuth, higherAzimuth,
                    lowerElevation, higherElevation,
                    moonshot.getMinMoonIllumination()
            );

            if (moonMoments != null && !moonMoments.isEmpty()) {
                return new DateTime(moonMoments.get(0).getDate());
            } else {
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        @Override
        protected void onPostExecute(DateTime targetDate) {
            Moonshot insertedMoonshot = moonshotDao.findMoonshotById(id);
            insertedMoonshot.setProcessing(false);
            //DateTime targetDate = new DateTime().plusDays(4).plusHours(7).plusMinutes(4);
            insertedMoonshot.setTargetDate(targetDate);
            moonshotDao.updateMoonshot(insertedMoonshot);
        }
    }
}
