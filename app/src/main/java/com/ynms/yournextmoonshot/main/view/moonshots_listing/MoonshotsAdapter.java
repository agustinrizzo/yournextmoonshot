package com.ynms.yournextmoonshot.main.view.moonshots_listing;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.ynms.yournextmoonshot.R;
import com.ynms.yournextmoonshot.main.model.Moonshot;

import static com.ynms.yournextmoonshot.moonshot.Utils.getCorrectOrientationBitmap;

public class MoonshotsAdapter extends ListAdapter<Moonshot, MoonshotViewHolder> {

    private static final String LOGTAG = MoonshotsAdapter.class.getSimpleName();

    private OnHolderButtonClickListener onHolderButtonClickListener;

    private static final DiffUtil.ItemCallback<Moonshot> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Moonshot>() {
                @Override
                public boolean areItemsTheSame(@NonNull Moonshot oldItem, @NonNull Moonshot newItem) {
                    return false;
                }

                @Override
                public boolean areContentsTheSame(@NonNull Moonshot oldItem, @NonNull Moonshot newItem) {
                    return false;
                }
            };

    MoonshotsAdapter() {
        super(DIFF_CALLBACK);
    }

    @NonNull
    @Override
    public MoonshotViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.moonshot_item_layout, parent, false);
        return new MoonshotViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MoonshotViewHolder holder, int position) {
        Moonshot moonshot = getItem(position);

        holder.textViewTitle.setText(moonshot.getName());
        holder.imageViewPhoto.setImageBitmap(getCorrectOrientationBitmap(moonshot.getCapturePath()));

        if (moonshot.getProcessing()) {
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.countdownView.setVisibility(View.GONE);
        } else {
            holder.progressBar.setVisibility(View.GONE);
            holder.countdownView.setVisibility(View.VISIBLE);
            holder.initCountdownView(moonshot.getTargetDate().getMillis());
        }

        holder.imageViewProperties.setOnClickListener(v -> {
            onHolderButtonClickListener.onHolderPropertiesButtonClick();
        });

        holder.imageViewLocation.setOnClickListener(v -> {
            Uri gmmIntentUri = Uri.parse(String.format("google.navigation:q=%f,%f",
                    -34.584033/*moonshot.getLatitude()*/, -58.409081/*moonshot.getLongitude()*/));
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            onHolderButtonClickListener.onHolderLocationButtonClick(mapIntent);
        });

        holder.imageViewShare.setOnClickListener(v -> {
            //obtainShortLinkUri
        });

        /*
        holder.textViewTitle.setText(Html.fromHtml(newArticle.getTitle()));
        if (!TextUtils.isEmpty(newArticle.getSubtitle())) {
            holder.textViewSubtitle.setText(Html.fromHtml(newArticle.getSubtitle()));
        }
        */
    }

    /*
    protected LiveData<Uri> obtainShortLinkUri(String articleTarget, final String title, final String subtitle, String imageName) {
        final MutableLiveData<Uri> shortLinkUriLiveData = new MutableLiveData<>();

        Utils.createDynamicLink(
                articleTarget,
                getApplication().getString(R.string.dynamic_link_domain),
                getApplication().getPackageName(),
                title, subtitle, imageUri,
                new Utils.OnCreateDynamicLinkListener() {
                    @Override
                    public void onCreate(Uri dynamicLinkUri) {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, "" + shortLinkUri);
                        sendIntent.setType("text/plain");
                        onHolderButtonClickListener.onHolderShareButtonClick(sendIntent);
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e(LOGTAG, "Error durante la generación del link dinámico", e);
                        shortLinkUriLiveData.setValue(null);
                    }
                });

        return shortLinkUriLiveData;
    }
    */

    public void setOnHolderButtonClickListener(OnHolderButtonClickListener onHolderButtonClickListener) {
        this.onHolderButtonClickListener = onHolderButtonClickListener;
    }

    public interface OnHolderButtonClickListener {
        void onHolderPropertiesButtonClick();
        void onHolderLocationButtonClick(Intent startActivityIntent);
        void onHolderShareButtonClick(Intent sendIntent);
    }
}
