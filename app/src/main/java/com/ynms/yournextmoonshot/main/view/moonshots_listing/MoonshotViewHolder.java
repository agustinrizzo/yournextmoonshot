package com.ynms.yournextmoonshot.main.view.moonshots_listing;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ynms.yournextmoonshot.R;
import com.ynms.yournextmoonshot.main.view.countdown.CountdownView;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MoonshotViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.textViewTitle)
    TextView textViewTitle;

    @BindView(R.id.imageViewPhoto)
    ImageView imageViewPhoto;

    @BindView(R.id.imageViewProperties)
    ImageView imageViewProperties;

    @BindView(R.id.imageViewLocation)
    ImageView imageViewLocation;

    @BindView(R.id.imageViewShare)
    ImageView imageViewShare;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.countdownView)
    CountdownView countdownView;

    public MoonshotViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        initGui();
    }

    private void initGui() {
        progressBar.setIndeterminate(true);
        //initCountdownView();
    }

    public void initCountdownView(long targetMilliseconds) {
        //LocalDateTime localDateTime = new LocalDateTime(2019,10, 19, 19, 23, 0);
        //long diffInMs = localDateTime.toDateTime().getMillis() - System.currentTimeMillis();
        long diffInMs = targetMilliseconds - new DateTime().getMillis();
        countdownView.start(diffInMs);
    }
}