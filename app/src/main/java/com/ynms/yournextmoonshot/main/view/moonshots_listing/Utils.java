package com.ynms.yournextmoonshot.main.view.moonshots_listing;

import android.net.Uri;

import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;


public class Utils {

    private static final String PROTOCOL = "https://";
    private static final String LINK_URL = PROTOCOL + "www.tierradelfuego.gov.ar?target=";
    private static final String DOMAIN_FIXED_PART = ".page.link";
    //private static final String IOS_PACKAGE_NAME = "ar.gob.tierradelfuego.mitdf";
    //private static final String APP_STORE_ID = "1458776117";


    public static void createDynamicLink(String articleTarget, String dynamicLinkDomain, String packageName,
                                         String title, String subtitle, Uri imageUri,
                                         OnCreateDynamicLinkListener onCreateDynamicLinkListener) {
        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(LINK_URL + articleTarget))
                .setDomainUriPrefix(PROTOCOL + dynamicLinkDomain + DOMAIN_FIXED_PART)
                .setAndroidParameters(
                        new DynamicLink.AndroidParameters.Builder(packageName)
                                .build())
                /*
                .setIosParameters(
                        new DynamicLink.IosParameters.Builder(IOS_PACKAGE_NAME)
                                .setAppStoreId(APP_STORE_ID)
                                .build())
                */
                .setSocialMetaTagParameters(
                        new DynamicLink.SocialMetaTagParameters.Builder()
                                .setTitle(title)
                                .setDescription(subtitle)
                                .setImageUrl(imageUri)
                                .build())
                .buildShortDynamicLink(ShortDynamicLink.Suffix.SHORT)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        if (task.getResult() != null) {
                            onCreateDynamicLinkListener.onCreate(task.getResult().getShortLink());
                        } else {
                            onCreateDynamicLinkListener.onError(null);
                        }
                    } else {
                        onCreateDynamicLinkListener.onError(task.getException());
                    }
                });
    }

        /*
    private void createDynamicLink(final MutableLiveData<Uri> shortLinkUriLiveData, String title,
                                   String subtitle, Uri imageUri) {
        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://www.mitdf.com?target=alert"))
                .setDomainUriPrefix("https://" + getApplication().getString(R.string.dynamic_link_domain) + ".page.link")
                .setAndroidParameters(
                        new DynamicLink.AndroidParameters.Builder(getApplication().getPackageName())
                                .build())
                .setIosParameters(
                        new DynamicLink.IosParameters.Builder("com.tablerotdf")
                                .setAppStoreId("123456789")
                                .build())
                .setSocialMetaTagParameters(
                        new DynamicLink.SocialMetaTagParameters.Builder()
                                .setTitle(title)
                                .setDescription(subtitle)
                                .setImageUrl(imageUri)
                                .build())
                .buildShortDynamicLink(ShortDynamicLink.Suffix.SHORT)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        shortLinkUriLiveData.setValue(task.getResult().getShortLink());
                    }
                    else {
                        Log.e(LOGTAG, "Error durante la generación del link dinámico", task.getException());
                    }
                });
    }
    */

    public interface OnCreateDynamicLinkListener {
        void onCreate(Uri dynamicLinkUri);
        void onError(Exception e);
    }
}
