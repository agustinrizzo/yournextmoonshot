package com.ynms.yournextmoonshot.main.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface MoonshotDao {

    // Devuelve todos los moonshots
    @Query("SELECT * FROM moonshot")
    LiveData<List<Moonshot>> getAllMoonshotsLiveData();

    @Insert(onConflict = REPLACE)
    long insertMoonshot(Moonshot moonshot);

    @Update(onConflict = REPLACE)
    int updateMoonshot(Moonshot moonshot);

    @Query("SELECT * FROM moonshot WHERE id = :id LIMIT 1")
    Moonshot findMoonshotById(Long id);

    @Query("DELETE FROM moonshot")
    void nukeTable();
}
