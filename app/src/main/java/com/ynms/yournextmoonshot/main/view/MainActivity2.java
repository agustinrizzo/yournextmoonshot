package com.ynms.yournextmoonshot.main.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.ynms.yournextmoonshot.R;
import com.ynms.yournextmoonshot.main.model.Moonshot;
import com.ynms.yournextmoonshot.main.view.moonshots_listing.MoonshotsRecyclerView;

import java.util.LinkedList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity2 extends AppCompatActivity {

    private static final String LOGTAG = MainActivity2.class.getSimpleName();

    @BindView(R.id.capturesRecyclerView)
    MoonshotsRecyclerView moonshotsRecyclerView;

    @BindView(R.id.buttonToTheMoon)
    ExtendedFloatingActionButton buttonToTheMoon;

    private LinkedList<Moonshot> moonshots = new LinkedList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);

        initGui();
    }

    LinkedList<Moonshot> asdfasdf = new LinkedList<>();

    private void initGui() {
        initRecyclerView();
        buttonToTheMoon.setOnClickListener(v -> {
            asdfasdf.add(new Moonshot()/*moonshot*/);
            moonshotsRecyclerView.submitCaptures(asdfasdf/*moonshots*/);
            /*
            Intent intent = new Intent(this, CaptureActivity.class);
            startActivityForResult(intent, 1);
            */
        });
        //initC();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    Moonshot moonshot = (Moonshot) bundle.getSerializable("moonshot");
                    moonshots.add(new Moonshot()/*moonshot*/);
                    moonshotsRecyclerView.submitCaptures(moonshots);
                }
            } else if (resultCode == 0) {
                Log.d(LOGTAG, "RESULT CANCELLED");
            }
        }
    }

    private void initRecyclerView() {
        moonshots.add(new Moonshot());
        moonshotsRecyclerView.submitCaptures(moonshots);

        moonshots.add(new Moonshot()/*moonshot*/);
        moonshotsRecyclerView.submitCaptures(moonshots);
    }
}
