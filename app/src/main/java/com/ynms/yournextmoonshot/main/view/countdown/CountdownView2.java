package com.ynms.yournextmoonshot.main.view.countdown;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.card.MaterialCardView;
import com.ynms.yournextmoonshot.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CountdownView2 extends MaterialCardView {

    private static final int COUNT_DOWN_INTERVAL = 1000;

    @BindView(R.id.textViewDays)
    TextView textViewDays;

    @BindView(R.id.textViewHours)
    TextView textViewHours;

    @BindView(R.id.textViewMinutes)
    TextView textViewMinutes;

    @BindView(R.id.textViewSeconds)
    TextView textViewSeconds;

    public CountdownView2(Context context) {
        super(context);
        init(context);
    }

    public CountdownView2(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CountdownView2(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.countdown_layout, this, true);
        ButterKnife.bind(view);
    }

    public void start(long diffInMs) {
        new CountDownTimer(diffInMs, COUNT_DOWN_INTERVAL) {
            public void onTick(long millisUntilFinished) {
                textViewDays.setText(Countdown.getDays(millisUntilFinished));
                textViewHours.setText(Countdown.getHours(millisUntilFinished));
                textViewMinutes.setText(Countdown.getMinutes(millisUntilFinished));
                textViewSeconds.setText(Countdown.getSeconds(millisUntilFinished));
            }

            public void onFinish() {
                //editTextCountdown.setText("done!");
            }
        }.start();
    }
}
