package com.ynms.yournextmoonshot.main.view.moonshots_listing;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ynms.yournextmoonshot.main.model.Moonshot;

import java.util.LinkedList;
import java.util.List;


public class MoonshotsRecyclerView extends RecyclerView {

    private static final String LOGTAG = MoonshotsRecyclerView.class.getSimpleName();

    private MoonshotsAdapter moonshotsAdapter;


    public MoonshotsRecyclerView(@NonNull Context context) {
        super(context);
        init();
    }

    public MoonshotsRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MoonshotsRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setHasFixedSize(true);
        initLayoutManager();
        initAdapter();
    }

    private void initLayoutManager() {
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        linearLayoutManager.setStackFromEnd(false);
        setLayoutManager(linearLayoutManager);
    }

    private void initAdapter() {
        moonshotsAdapter = new MoonshotsAdapter();
        setAdapter(moonshotsAdapter);
    }

    public void submitCaptures(List<Moonshot> moonshots) {
        moonshotsAdapter.submitList(moonshots);
    }

    public void setOnHolderButtonClickListener(
            MoonshotsAdapter.OnHolderButtonClickListener onHolderButtonClickListener) {
        moonshotsAdapter.setOnHolderButtonClickListener(onHolderButtonClickListener);
    }

}
