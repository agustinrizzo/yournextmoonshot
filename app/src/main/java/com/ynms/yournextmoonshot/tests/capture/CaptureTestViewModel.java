package com.ynms.yournextmoonshot.tests.capture;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.ynms.yournextmoonshot.model.moon_moments.MoonMomentsRepository;
import com.ynms.yournextmoonshot.model.moon_moments.moon_moment.MoonMoment;
import com.ynms.yournextmoonshot.sensors.Orientation;
import com.ynms.yournextmoonshot.sensors.OrientationRepository;
import com.ynms.yournextmoonshot.viewmodel.MoonshotViewModel;

public class CaptureTestViewModel extends AndroidViewModel {

    private static final String LOGTAG = MoonshotViewModel.class.getSimpleName();

    private OrientationRepository orientationRepository;
    private MoonMomentsRepository moonMomentsRepository;

    private String capturePath;
    public String getCapturePath() {
        return capturePath;
    }
    public void setCapturePath(String capturePath) {
        this.capturePath = capturePath;
    }

    private LiveData<Orientation> orientationLiveData;
    public LiveData<Orientation> getOrientationLiveData() {
        return orientationLiveData;
    }

    public CaptureTestViewModel(@NonNull Application application) {
        super(application);

        orientationRepository = new OrientationRepository(getApplication());
        orientationLiveData = orientationRepository.getOrientationLiveData();

        moonMomentsRepository = new MoonMomentsRepository(application);
        Log.d(LOGTAG, "MOON MOMENTS QUANTITY: " + moonMomentsRepository.getMoonMomentsCount());
    }

    public void resumeOrientation() {
        orientationRepository.resume();
    }

    public void pauseOrientation() {
        orientationRepository.pause();
    }

    public MoonMoment getCurrentMoonMoment() {
        return moonMomentsRepository.getCurrentMoonMoment();
    }

    public MoonMoment getNearestMoonMoment() {
        return moonMomentsRepository.getNearestMoonMoment();
    }
}
