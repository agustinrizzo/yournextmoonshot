/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ynms.yournextmoonshot.tests.capture;

import android.app.Activity;
import android.content.Intent;
import android.hardware.GeomagneticField;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.ynms.yournextmoonshot.R;
import com.ynms.yournextmoonshot.Utils;
import com.ynms.yournextmoonshot.camera.Fov;
import com.ynms.yournextmoonshot.capture.view.CameraFragment;
import com.ynms.yournextmoonshot.capture.viewmodel.CaptureViewModel;
import com.ynms.yournextmoonshot.main.model.Moonshot;
import com.ynms.yournextmoonshot.model.moon_moments.moon_moment.MoonMoment;
import com.ynms.yournextmoonshot.moonshot.MoonshotSetPropertiesActivity;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CaptureTestActivity extends AppCompatActivity {

    private static final String LOGTAG = CaptureTestActivity.class.getSimpleName();

    @BindView(R.id.textViewHeading)
    TextView textViewHeading;
    @BindView(R.id.imageViewHeading)
    ImageView imageViewHeading;

    @BindView(R.id.textViewElevation)
    TextView textViewElevation;
    @BindView(R.id.imageViewElevation)
    ImageView imageViewElevation;

    @BindView(R.id.textViewCurrentData)
    TextView textViewCurrentData;

    private CaptureTestViewModel captureTestViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.capture_test_activity);
        ButterKnife.bind(this);

        getSupportActionBar().hide();

        initGui(savedInstanceState);
        initViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        captureTestViewModel.resumeOrientation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        captureTestViewModel.pauseOrientation();
    }

    private void initGui(Bundle savedInstanceState) {
        if (null == savedInstanceState) {
            CameraFragment cameraFragment = new CameraFragment();
            cameraFragment.setOnPhotoTakenListener(new CameraFragment.OnPhotoTakenListener() {
                @Override
                public void stopOrientation() {
                    captureTestViewModel.pauseOrientation();
                }

                @Override
                public void onPhotoTaken(@NotNull String capturePath) {
                    captureTestViewModel.setCapturePath(capturePath);
                    /*
                    hide();

                    MoonshotSetPropertiesFragment moonshotSetPropertiesFragment =
                            new MoonshotSetPropertiesFragment();
                    moonshotSetPropertiesFragment.setCapture(captureFileName);

                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, moonshotSetPropertiesFragment)
                            .commit();
                    */

                    Intent openActivityIntent = new Intent(
                            CaptureTestActivity.this, MoonshotSetPropertiesActivity.class);
                    openActivityIntent.putExtra("capturePath", capturePath);
                    startActivityForResult(openActivityIntent, 2);

                    /*
                    Intent intent = new Intent();
                    intent.putExtra("captureFileName", captureFileName);
                    intent.putExtra("orientation", orientation);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                    */
                }
            });

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, cameraFragment)
                    .commit();
        }
    }

    /*
    private void hide() {
        textViewHeading.setVisibility(View.INVISIBLE);
        imageViewHeading.setVisibility(View.INVISIBLE);
        textViewElevation.setVisibility(View.INVISIBLE);
        imageViewElevation.setVisibility(View.INVISIBLE);
    }
    */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    Moonshot moonshot = new Moonshot(
                            data.getStringExtra("name"),
                            0.0f, 0.0f, captureTestViewModel.getOrientationLiveData().getValue(),
                            data.getFloatExtra("minMoonIllumination", 1.0f),
                            data.getFloatExtra("minSkyDarkness", 1.0f),
                            data.getIntExtra("lens", 1),
                            captureTestViewModel.getCapturePath(),
                            new DateTime(),
                            new Fov(1.0f, 1.0f)
                    );
                    moonshot.setTargetDate(new DateTime().plusDays(4).plusHours(7).plusMinutes(4));

                    Intent intent = new Intent();
                    intent.putExtra("moonshot", moonshot);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            } else if (resultCode == 0) {
                Log.d(LOGTAG, "RESULT CANCELLED");
            }
        }
    }

    private void initViewModel() {
        captureTestViewModel = ViewModelProviders.of(this).get(CaptureTestViewModel.class);

        captureTestViewModel.getOrientationLiveData().observe(this, orientation -> {
            textViewHeading.setText(String.format("%d° (%d°)", (int) orientation.getAzimuth(), (int) getCorrectedAzimuth(-34.548787f, -58.443638f, orientation.getAzimuth())));
            //textViewHeading.setText(String.format("%d°", (int) orientation.getAzimuth()));
            textViewElevation.setText(String.format("%d°", (int) orientation.getElevation()));
        });

        MoonMoment currentMoonMoment = captureTestViewModel.getCurrentMoonMoment();
        StringBuilder stringBuilder = new StringBuilder();
        if (currentMoonMoment != null) {
            stringBuilder.append("Date: " + Utils.getFullDateString(new DateTime(currentMoonMoment.getDate())));
            stringBuilder.append(System.getProperty("line.separator"));
            stringBuilder.append("Azimuth: " + currentMoonMoment.getAzimuth());
            stringBuilder.append(System.getProperty("line.separator"));
            stringBuilder.append("Altitude: " + currentMoonMoment.getAltitude());
            stringBuilder.append(System.getProperty("line.separator"));
            stringBuilder.append("FractionIlluminated: " + currentMoonMoment.getFractionIlluminated());
        } else {
            stringBuilder.append("En este momento la Luna no es visible");
            stringBuilder.append(System.getProperty("line.separator"));
            MoonMoment nearestMoonMoment = captureTestViewModel.getNearestMoonMoment();
            if (nearestMoonMoment != null) {
                stringBuilder.append("Vas a poder verla el: " +
                        Utils.getFullDateString(new DateTime(nearestMoonMoment.getDate())));
            }
        }
        textViewCurrentData.setText(stringBuilder);
    }

    private float getCorrectedAzimuth(float latitude, float longitude, float azimuth) {
        GeomagneticField geomagneticField =
                new GeomagneticField(latitude, longitude, 0, new DateTime().getMillis());
        Log.d(LOGTAG, "Declinación magnética: " + geomagneticField.getDeclination());

        azimuth += geomagneticField.getDeclination();
        if (azimuth < 0) {
            azimuth += 360;
        }
        return azimuth;
    }
}
