package com.ynms.yournextmoonshot.tests;

import android.content.pm.PackageManager;
import android.hardware.GeomagneticField;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import com.ynms.yournextmoonshot.R;
import com.ynms.yournextmoonshot.Utils;
import com.ynms.yournextmoonshot.model.moon_moments.moon_moment.MoonMoment;
import com.ynms.yournextmoonshot.viewmodel.MoonshotViewModel;

import org.joda.time.DateTime;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TestActivity extends AppCompatActivity {

    private static final String LOGTAG = TestActivity.class.getSimpleName();

    private int REQUEST_CODE_PERMISSIONS = 101;
    private final String[] REQUIRED_PERMISSIONS =
            new String[]{
                    "android.permission.WRITE_EXTERNAL_STORAGE",
                    "android.permission.CAMERA"
    };

    @BindView(R.id.textView)
    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity);
        ButterKnife.bind(this);

        if (allPermissionsGranted()){
            initViewModel();
        } else{
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                initViewModel();
            } else {
                Toast.makeText(this, "Permissions not granted by the user.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private boolean allPermissionsGranted() {
        for (String permission : REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    private void initViewModel() {
        MoonshotViewModel moonshotViewModel = ViewModelProviders.of(this).get(MoonshotViewModel.class);

        moonshotViewModel.getLoadedLiveData().observe(this, aBoolean -> {
            Toast.makeText(this, "Base de datos CARGADA!!!", Toast.LENGTH_LONG).show();
            //moonshotViewModel.getAllMoonMoments();
        });

        moonshotViewModel.getExportedLiveData().observe(this, aBoolean -> {
            Toast.makeText(this, "Base de datos EXPORTADA!!!", Toast.LENGTH_LONG).show();
        });

        moonshotViewModel.getMoonMomentsLiveData().observe(this, moonMoments -> {
            StringBuilder stringBuilder = new StringBuilder();
            for (MoonMoment moonMoment : moonMoments) {
                stringBuilder.append(String.format("%d %f %f", moonMoment.getDate(), moonMoment.getLatitude(), moonMoment.getLongitude()));
                stringBuilder.append(System.getProperty("line.separator"));
            }
            textView.setText(stringBuilder.toString());
        });

        /*
        Moonshot capture = new Moonshot();
        moonshotViewModel.getMoonMomentsForMoonshot(capture);
        */

        MoonMoment currentMoonMoment = moonshotViewModel.getCurrentMoonMoment();
        if (currentMoonMoment != null) {
            Log.d(LOGTAG, "--------------------------------------------------------------");
            Log.d(LOGTAG, "Date: " + Utils.getFullDateString(new DateTime(currentMoonMoment.getDate())));
            Log.d(LOGTAG, "Azimuth: " + currentMoonMoment.getAzimuth());
            Log.d(LOGTAG, "Altitude: " + currentMoonMoment.getAltitude());
            Log.d(LOGTAG, "FractionIlluminated: " + currentMoonMoment.getFractionIlluminated());
            Log.d(LOGTAG, "--------------------------------------------------------------");
        } else {
            Log.d(LOGTAG, "En este momento la Luna no es visible");
            MoonMoment nearestMoonMoment = moonshotViewModel.getNearestMoonMoment();
            if (nearestMoonMoment != null) {
                Log.d(LOGTAG, "Vas a poder verla el: " +
                        Utils.getFullDateString(new DateTime(nearestMoonMoment.getDate())));
            }
        }

        Log.d(LOGTAG, "AZIMUTH: " +
                getCorrectedAzimuth(-34.548787f, -58.443638f, 5.0f));
    }

    private float getCorrectedAzimuth(float latitude, float longitude, float azimuth) {
        GeomagneticField geomagneticField =
                new GeomagneticField(latitude, longitude, 0, new DateTime().getMillis());
        Log.d(LOGTAG, "Declinación magnética: " + geomagneticField.getDeclination());

        azimuth += geomagneticField.getDeclination();
        if (azimuth < 0) {
            azimuth += 360;
        }
        return azimuth;
    }

    /*
    public void setLocation(Location location)
    {
        GeomagneticField geomagneticField = new GeomagneticField(
                (float) location.getLatitude(),
                (float) location.getLongitude(),
                (float) location.getAltitude(),
                System.currentTimeMillis());
        declination = (float) Math.toRadians(geomagneticField.getDeclination());
    }
    */
}
