package com.ynms.yournextmoonshot.tests;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.ynms.yournextmoonshot.R;
import com.ynms.yournextmoonshot.Utils;
import com.ynms.yournextmoonshot.model.moon_moments.moon_moment.MoonMoment;
import com.ynms.yournextmoonshot.moonshot.MoonshotSetPropertiesDialog;
import com.ynms.yournextmoonshot.sensors.Orientation;
import com.ynms.yournextmoonshot.sensors.SensorActivity;
import com.ynms.yournextmoonshot.viewmodel.MoonshotViewModel;

import org.joda.time.DateTime;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MoonMachingTestActivity2 extends AppCompatActivity {

    private static final String LOGTAG = SensorActivity.class.getSimpleName();

    // GUI
    @BindView(R.id.textViewAzimuth)
    TextView textViewAzimuth;

    @BindView(R.id.textViewElevation)
    TextView textViewElevation;

    @BindView(R.id.textViewCurrentMoonMoment)
    TextView textViewCurrentMoonMoment;

    @BindView(R.id.buttonShot)
    Button buttonShot;

    // ViewModel
    private MoonshotViewModel moonshotViewModel;

    // Data
    private Orientation orientation;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.moon_maching_test_activity);
        ButterKnife.bind(this);

        initViewModel();

        buttonShot.setOnClickListener(v -> {
            /*
            moonshotViewModel.getMoonMomentsForMoonshot(
                    new Moonshot(null, this.orientation, "", 0.8f,
                            null, new DateTime(), CameraUtils.calculateFOV(this))
            );
            */

            /*
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            ViewGroup viewGroup = findViewById(android.R.id.content);
            View dialogView = LayoutInflater.from(v.getContext()).inflate(R.layout.capture_set_properties_layout, viewGroup, false);
            builder.setView(dialogView);
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            */
            initCaptureConfigFragment();
        });
    }

    private void initCaptureConfigFragment() {
        final MoonshotSetPropertiesDialog moonshotSetPropertiesDialog = new MoonshotSetPropertiesDialog();
        moonshotSetPropertiesDialog.setOnDatabaseLoadedListener(
                () -> {
                    moonshotSetPropertiesDialog.dismiss();
                    //startActivity(openMainActivityIntent);
                    finish();
                }
        );
        moonshotSetPropertiesDialog.show(getSupportFragmentManager(), "capture_config_fragment");
    }

    @Override
    protected void onResume() {
        super.onResume();
        moonshotViewModel.resumeOrientation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        moonshotViewModel.pauseOrientation();
    }

    private void initViewModel() {
        moonshotViewModel = ViewModelProviders.of(this).get(MoonshotViewModel.class);

        moonshotViewModel.getOrientationLiveData().observe(this, orientation -> {
            this.orientation = orientation;

            buttonShot.setEnabled(true);

            textViewAzimuth.setText(Integer.toString((int) orientation.getAzimuth()));
            textViewElevation.setText(Integer.toString((int) orientation.getElevation()));
        });

        MoonMoment currentMoonMoment = moonshotViewModel.getCurrentMoonMoment();
        StringBuilder stringBuilder = new StringBuilder();
        if (currentMoonMoment != null) {
            stringBuilder.append("Date: " + Utils.getFullDateString(new DateTime(currentMoonMoment.getDate())));
            stringBuilder.append(System.getProperty("line.separator"));
            stringBuilder.append("Azimuth: " + currentMoonMoment.getAzimuth());
            stringBuilder.append(System.getProperty("line.separator"));
            stringBuilder.append("Altitude: " + currentMoonMoment.getAltitude());
            stringBuilder.append(System.getProperty("line.separator"));
            stringBuilder.append("FractionIlluminated: " + currentMoonMoment.getFractionIlluminated());
        } else {
            stringBuilder.append("En este momento la Luna no es visible");
            stringBuilder.append(System.getProperty("line.separator"));
            MoonMoment nearestMoonMoment = moonshotViewModel.getNearestMoonMoment();
            if (nearestMoonMoment != null) {
                stringBuilder.append("Vas a poder verla el: " +
                        Utils.getFullDateString(new DateTime(nearestMoonMoment.getDate())));
            }
        }
        textViewCurrentMoonMoment.setText(stringBuilder);
    }
}
