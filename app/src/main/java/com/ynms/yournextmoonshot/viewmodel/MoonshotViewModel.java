package com.ynms.yournextmoonshot.viewmodel;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ynms.yournextmoonshot.main.model.Moonshot;
import com.ynms.yournextmoonshot.model.moon_moments.MoonMomentsRepository;
import com.ynms.yournextmoonshot.model.moon_moments.moon_moment.MoonMoment;
import com.ynms.yournextmoonshot.sensors.Orientation;
import com.ynms.yournextmoonshot.sensors.OrientationRepository;

import org.joda.time.DateTime;

import java.util.List;


public class MoonshotViewModel extends AndroidViewModel {

    private static final String LOGTAG = MoonshotViewModel.class.getSimpleName();

    private OrientationRepository orientationRepository;
    private MoonMomentsRepository moonMomentsRepository;

    private LiveData<Boolean> loadedLiveData;
    public LiveData<Boolean> getLoadedLiveData() {
        return loadedLiveData;
    }

    private LiveData<Boolean> exportedLiveData;
    public LiveData<Boolean> getExportedLiveData() {
        return exportedLiveData;
    }

    private LiveData<Orientation> orientationLiveData;
    public LiveData<Orientation> getOrientationLiveData() {
        return orientationLiveData;
    }

    private MutableLiveData<List<MoonMoment>> moonMomentsLiveData = new MutableLiveData<>();
    public LiveData<List<MoonMoment>> getMoonMomentsLiveData() {
        return moonMomentsLiveData;
    }

    private MutableLiveData<DateTime> moonshotDateLiveData = new MutableLiveData<>();
    public LiveData<DateTime> getMoonshotDateLiveData() {
        return moonshotDateLiveData;
    }


    public MoonshotViewModel(@NonNull Application application) {
        super(application);

        moonMomentsRepository = new MoonMomentsRepository(application);

        //moonMomentsRepository.startMoonMomentsFetch();
        //moonMomentsRepository.startMoonMomentsLoad();
        loadedLiveData = moonMomentsRepository.getLoadedLiveData();

        //moonMomentsRepository.exportDataBaseToCsv();
        exportedLiveData = moonMomentsRepository.getExportedLiveData();

        Log.d(LOGTAG, "MOON MOMENTS QUANTITY: " + moonMomentsRepository.getMoonMomentsCount());

        orientationRepository = new OrientationRepository(getApplication());
        orientationLiveData = orientationRepository.getOrientationLiveData();
    }

    public void resumeOrientation() {
        orientationRepository.resume();
    }

    public void pauseOrientation() {
        orientationRepository.pause();
    }

    public void getAllMoonMoments() {
        moonMomentsLiveData.setValue(moonMomentsRepository.getAllMoonMoments());
    }

    public void getMoonMomentsForMoonshot(Moonshot moonshot) {
        ComputeMoonshotAsyncTask computeMoonshotAsyncTask = new ComputeMoonshotAsyncTask(moonshot);
        computeMoonshotAsyncTask.execute();
    }

    private class ComputeMoonshotAsyncTask extends AsyncTask<Void, Integer, DateTime> {

        private Moonshot moonshot;

        ComputeMoonshotAsyncTask(Moonshot moonshot) {
            this.moonshot = moonshot;
        }

        @Override
        protected DateTime doInBackground(Void... params) {
            float captureAzimuth = moonshot.getOrientation().getAzimuth();
            float lowerAzimuth = captureAzimuth - (moonshot.getFov().getHorizonalAngle() / 2);
            float higherAzimuth = captureAzimuth + (moonshot.getFov().getHorizonalAngle() / 2);

            float captureElevation = moonshot.getOrientation().getElevation();
            float lowerElevation = captureElevation - (moonshot.getFov().getVerticalAngle() / 2);
            if (lowerElevation < 0) {
                lowerElevation = 0;
            }
            float higherElevation = captureElevation + (moonshot.getFov().getVerticalAngle() / 2);

            List<MoonMoment> moonMoments = moonMomentsRepository.getRestrictedMoonMoments(
                    moonshot.getCaptureDate().getMillis(),
                    lowerAzimuth, higherAzimuth,
                    lowerElevation, higherElevation,
                    moonshot.getMinMoonIllumination()
            );

            if (moonMoments != null && !moonMoments.isEmpty()) {
                return new DateTime(moonMoments.get(0).getDate());
            } else {
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        @Override
        protected void onPostExecute(DateTime result) {
            moonshotDateLiveData.setValue(result);
        }
    }

    public MoonMoment getCurrentMoonMoment() {
        return moonMomentsRepository.getCurrentMoonMoment();
    }

    public MoonMoment getNearestMoonMoment() {
        return moonMomentsRepository.getNearestMoonMoment();
    }
}
