package com.ynms.yournextmoonshot.moonshot;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.ynms.yournextmoonshot.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MoonshotViewPropertiesDialog extends DialogFragment {

    //private static final String LOGTAG = DatabaseLoadingDialogFragment.class.getSimpleName();

    @BindView(R.id.imageViewClose)
    ImageView imageViewClose;

    @BindView(R.id.editTextMoonshotTitle)
    EditText editTextMoonshotTitle;

    @BindView(R.id.imageViewIllumination)
    ImageView imageViewIllumination;
    @BindView(R.id.textViewIllumination)
    TextView textViewIllumination;

    @BindView(R.id.imageViewClarity)
    ImageView imageViewClarity;
    @BindView(R.id.textViewClarity)
    TextView textViewClarity;

    private OnCloseListener onCloseListener;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
    }

    public void setOnCloseListener(OnCloseListener onCloseListener) {
        this.onCloseListener = onCloseListener;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.capture_view_properties_layout, container, false);
        ButterKnife.bind(this, v);
        //getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        //initIlluminationControl();
        //initClarityControl();
        imageViewClose.setOnClickListener(v1 -> onCloseListener.onClose());
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    /*
    private void initIlluminationControl() {
        seekBarIllumination.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int backgroundResourceId = R.drawable.ic_brightness_3_grey_700_36dp;
                int illuminationLevel = 20;
                switch (progress) {
                    case 0:
                        backgroundResourceId = R.drawable.ic_brightness_3_grey_700_36dp;
                        illuminationLevel = 20;
                        break;
                    case 1:
                        backgroundResourceId = R.drawable.ic_brightness_2_grey_700_36dp;
                        illuminationLevel = 40;
                        break;
                    case 2:
                        backgroundResourceId = R.drawable.ic_brightness_1_grey_700_36dp;
                        illuminationLevel = 60;
                        break;
                    case 3:
                        backgroundResourceId = R.drawable.ic_brightness_1_grey_700_36dp;
                        illuminationLevel = 80;
                        break;
                }
                imageViewIllumination.setImageResource(backgroundResourceId);
                textViewIllumination.setText(illuminationLevel + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        seekBarIllumination.setProgress(1);
    }
    */

    /*
    private void initClarityControl() {
        seekBarClarity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int backgroundResourceId = R.drawable.ic_brightness_low_grey_700_36dp;
                int clarityLevel = 20;
                switch (progress) {
                    case 0:
                        backgroundResourceId = R.drawable.ic_brightness_low_grey_700_36dp;
                        clarityLevel = 20;
                        break;
                    case 1:
                        backgroundResourceId = R.drawable.ic_brightness_medium_grey_700_36dp;
                        clarityLevel = 40;
                        break;
                    case 2:
                        backgroundResourceId = R.drawable.ic_brightness_high_grey_700_36dp;
                        clarityLevel = 60;
                        break;
                    case 3:
                        backgroundResourceId = R.drawable.ic_brightness_high_grey_700_36dp;
                        clarityLevel = 80;
                        break;
                }
                imageViewClarity.setImageResource(backgroundResourceId);
                textViewClarity.setText(clarityLevel + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }
    */

    public interface OnCloseListener {
        void onClose();
    }
}