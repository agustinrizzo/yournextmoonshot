package com.ynms.yournextmoonshot.moonshot;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.button.MaterialButton;
import com.ynms.yournextmoonshot.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MoonshotResumeDialog extends DialogFragment {

    private static final String LOGTAG = MoonshotResumeDialog.class.getSimpleName();

    @BindView(R.id.checkBoxNoMore)
    CheckBox checkBoxNoMore;

    @BindView(R.id.buttonOk)
    MaterialButton buttonOk;

    private OnCloseListener onCloseListener;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
    }

    public void setOnCloseListener(OnCloseListener onCloseListener) {
        this.onCloseListener = onCloseListener;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.resume_dialog_layout, container, false);
        ButterKnife.bind(this, v);
        //getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        buttonOk.setOnClickListener(v1 -> onCloseListener.onClose());
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    public interface OnCloseListener {
        void onClose();
    }
}