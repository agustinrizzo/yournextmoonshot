package com.ynms.yournextmoonshot.sensors;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;


public class OrientationViewModel extends AndroidViewModel {

    private OrientationRepository orientationRepository;

    private LiveData<Orientation> orientationLiveData;
    public LiveData<Orientation> getOrientationLiveData() {
        return orientationLiveData;
    }

    public OrientationViewModel(@NonNull Application application) {
        super(application);

        orientationRepository = new OrientationRepository(getApplication());
        orientationLiveData = orientationRepository.getOrientationLiveData();
    }

    public void resumeOrientation() {
        orientationRepository.resume();
    }

    public void pauseOrientation() {
        orientationRepository.pause();
    }
}
