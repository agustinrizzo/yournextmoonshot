package com.ynms.yournextmoonshot.sensors;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.ynms.yournextmoonshot.camera.CameraUtils;
import com.ynms.yournextmoonshot.camera.Fov;
import com.ynms.yournextmoonshot.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SensorActivity extends AppCompatActivity {

    private static final String LOGTAG = SensorActivity.class.getSimpleName();

    // GUI
    @BindView(R.id.textViewAzimuth)
    TextView textViewAzimuth;

    @BindView(R.id.textViewElevation)
    TextView textViewElevation;

    private OrientationViewModel orientationViewModel;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.moon_maching_test_activity);
        ButterKnife.bind(this);

        initViewModel();

        Fov fov = CameraUtils.calculateFOV(this);
        Log.d(LOGTAG, "Horizonal Angle: " + fov.getHorizonalAngle());
        Log.d(LOGTAG, "Vertical Angle: " + fov.getVerticalAngle());
    }

    @Override
    protected void onResume() {
        super.onResume();
        orientationViewModel.resumeOrientation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        orientationViewModel.pauseOrientation();
    }

    private void initViewModel() {
        orientationViewModel = ViewModelProviders.of(this).get(OrientationViewModel.class);

        orientationViewModel.getOrientationLiveData().observe(this, orientation -> {
            textViewAzimuth.setText(Integer.toString((int) orientation.getAzimuth()));
            textViewElevation.setText(Integer.toString((int) orientation.getElevation()));
        });
    }
}
