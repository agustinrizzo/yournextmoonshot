package com.ynms.yournextmoonshot.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class OrientationRepository implements SensorEventListener {

    // Sensors Data
    private SensorManager sensorManager;

    //private final float[] accelerometerReading = new float[3];
    //private final float[] magnetometerReading = new float[3];

    private final float[] rotationMatrix = new float[9];
    private final float[] orientationAngles = new float[3];

    // Live Data
    private MutableLiveData<Orientation> orientationLiveData = new MutableLiveData<>();
    public LiveData<Orientation> getOrientationLiveData() {
        return orientationLiveData;
    }

    public OrientationRepository(Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
    }

    public void resume() {
        // Get updates from the accelerometer and magnetometer at a constant rate.
        // To make batch operations more efficient and reduce power consumption,
        // provide support for delaying updates to the application.
        //
        // In this example, the sensor reporting delay is small enough such that
        // the application receives an update before the system checks the sensor
        // readings again.
        Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (accelerometer != null) {
            sensorManager.registerListener(this, accelerometer,
                    SensorManager.SENSOR_DELAY_NORMAL, SensorManager.SENSOR_DELAY_UI);
        }
        Sensor magneticField = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        if (magneticField != null) {
            sensorManager.registerListener(this, magneticField,
                    SensorManager.SENSOR_DELAY_NORMAL, SensorManager.SENSOR_DELAY_UI);
        }

        Sensor rotationVector = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        if (rotationVector != null) {
            sensorManager.registerListener(this, rotationVector,
                    SensorManager.SENSOR_DELAY_FASTEST, SensorManager.SENSOR_DELAY_UI);
        }
        /*
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR);
        if (sensor != null) {
            sensorManager.registerListener(this, sensor,
                    SensorManager.SENSOR_DELAY_FASTEST, SensorManager.SENSOR_DELAY_UI);
        }
        */
    }

    public void pause() {
        // Don't receive any more updates from either sensor.
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            // calculate th rotation matrix
            SensorManager.getRotationMatrixFromVector( rotationMatrix, event.values );
            // get the azimuth value (orientation[0]) in degree

            float[] outRotationMatrix = new float[9];
            SensorManager.remapCoordinateSystem(
                    rotationMatrix,
                    SensorManager.AXIS_Z,
                    SensorManager.AXIS_X,
                    outRotationMatrix
            );

            SensorManager.getOrientation(outRotationMatrix, orientationAngles);

            float azimuthDegrees = (float) Math.toDegrees(orientationAngles[0]) + 180;
            float pichDegrees = (float) Math.toDegrees(orientationAngles[1]);

            orientationLiveData.setValue(new Orientation(azimuthDegrees, pichDegrees));
        }
        /*
        else if (event.sensor.getType() == Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR) {
            Log.d(LOGTAG, "lee");
        }
        */
    }

    // Get readings from accelerometer and magnetometer. To simplify calculations,
    // consider storing these readings as unit vectors.
    /*
    @Override
    public void onSensorChanged(SensorEvent event) {
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                System.arraycopy(event.values, 0, accelerometerReading,
                        0, accelerometerReading.length);
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                System.arraycopy(event.values, 0, magnetometerReading,
                        0, magnetometerReading.length);
                break;
        }

        updateOrientationAngles();
    }
    */

    // Compute the three orientation angles based on the most recent readings from
    // the device's accelerometer and magnetometer.
    /*
    public void updateOrientationAngles() {

        // Update rotation matrix, which is needed to update orientation angles.

        // https://developer.android.com/reference/android/hardware/SensorManager.html#getRotationMatrix(float%5B%5D,%20float%5B%5D,%20float%5B%5D,%20float%5B%5D)
        SensorManager.getRotationMatrix(rotationMatrix, null, accelerometerReading, magnetometerReading);
        // "mRotationMatrix" now has up-to-date information.

        float[] outRotationMatrix = new float[9];
        SensorManager.remapCoordinateSystem(
                rotationMatrix,
                SensorManager.AXIS_X,
                SensorManager.AXIS_Z,
                outRotationMatrix
        );

        // https://developer.android.com/reference/android/hardware/SensorManager.html#getOrientation(float[],%20float[])
        SensorManager.getOrientation(outRotationMatrix, orientationAngles);
        // "mOrientationAngles" now has up-to-date information.

        int azimuthDegrees = (int) (Math.toDegrees(orientationAngles[0]) + 360) % 360;
        int pichDegrees = (int) Math.toDegrees(orientationAngles[1]);

        // "mRotationMatrix" now has up-to-date information.
        textViewAzimuth.setText(Integer.toString(azimuthDegrees));
        //textViewAzimuth.setText(Double.toString(Math.toDegrees(orientationAngles[0])));
        textViewElevation.setText(Integer.toString(pichDegrees));
    }
    */

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
        // You must implement this callback in your code.
    }
}
