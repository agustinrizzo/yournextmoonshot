package com.ynms.yournextmoonshot.sensors;

import java.io.Serializable;


public class Orientation implements Serializable {

    private float azimuth;
    private float elevation;


    public Orientation() {
    }

    public Orientation(float azimuth, float elevation) {
        this.azimuth = azimuth;
        this.elevation = elevation;
    }

    public float getAzimuth() {
        return azimuth;
    }

    public void setAzimuth(float azimuth) {
        this.azimuth = azimuth;
    }

    public float getElevation() {
        return elevation;
    }

    public void setElevation(float elevation) {
        this.elevation = elevation;
    }
}
