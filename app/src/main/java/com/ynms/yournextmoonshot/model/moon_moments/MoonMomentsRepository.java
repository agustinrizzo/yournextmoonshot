package com.ynms.yournextmoonshot.model.moon_moments;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.ynms.yournextmoonshot.Utils;
import com.ynms.yournextmoonshot.model.database.AppDatabase;
import com.ynms.yournextmoonshot.model.moon_moments.daly_moon_raw_data.DalyMoonRawData;
import com.ynms.yournextmoonshot.model.moon_moments.daly_moon_raw_data.DalyMoonRawDataRequest;
import com.ynms.yournextmoonshot.model.moon_moments.moon_moment.MoonMoment;
import com.ynms.yournextmoonshot.model.moon_moments.moon_moment.MoonMomentDao;
import com.ynms.yournextmoonshot.model.moon_moments.moon_moment_data.MoonMomentData;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.LinkedList;
import java.util.List;


public class MoonMomentsRepository {

    private static final String LOGTAG = MoonMomentsRepository.class.getSimpleName();
    private static final int MOON_MOMENTS_CACHE_DAYS = 365;

    private MoonMomentDao moonMomentDao;

    private MutableLiveData<Boolean> loadedLiveData = new MutableLiveData();
    public MutableLiveData<Boolean> getLoadedLiveData() {
        return loadedLiveData;
    }

    private MutableLiveData<Boolean> exportedLiveData = new MutableLiveData();
    public MutableLiveData<Boolean> getExportedLiveData() {
        return exportedLiveData;
    }

    public MoonMomentsRepository(Context context) {
        moonMomentDao = AppDatabase.getAppDatabase(context).moonMomentDao();
    }

    public void startMoonMomentsFetch() {
        moonMomentDao.nukeTable();
        FetchAllNextMoonMomentsAsyncTask fetchAllNextMoonMomentsAsyncTask =
                new FetchAllNextMoonMomentsAsyncTask();
        fetchAllNextMoonMomentsAsyncTask.execute();
    }

    private class FetchAllNextMoonMomentsAsyncTask extends AsyncTask<Void, Integer, Void> {

        FetchAllNextMoonMomentsAsyncTask() {
        }

        @Override
        protected Void doInBackground(Void... params) {
            DateTime dateTime = new DateTime(); //new DateTime(2019, 9, 10, 0, 0);

            for (int daysCounter = 0; daysCounter < MOON_MOMENTS_CACHE_DAYS; daysCounter++) {
                DalyMoonRawDataRequest dalyMoonRawDataRequest = new DalyMoonRawDataRequest(dateTime, null, 10);
                LinkedList<MoonMomentData> moonMomentDataList = fetchMoonMoments(dalyMoonRawDataRequest);
                saveMoonMoments(moonMomentDataList, dalyMoonRawDataRequest);

                dateTime = dateTime.plusDays(1);
                Log.d(LOGTAG, String.format("dateTime: %s - moon moments: %d",
                        Utils.getFullDateString(dateTime), moonMomentDataList.size()));
            }
            return null;
        }

        private LinkedList<MoonMomentData> fetchMoonMoments(DalyMoonRawDataRequest dalyMoonRawDataRequest) {
            DalyMoonRawData dalyMoonRawData = new DalyMoonRawData(dalyMoonRawDataRequest);

            LinkedList<MoonMomentData> moonMomentDataList = new LinkedList<>();
            String[] lines = dalyMoonRawData.getLines();
            for (String line : lines) {
                String[] possibleMoonMomentData = dalyMoonRawData.getLineData(line);
                if (possibleMoonMomentData.length == 4 &&
                        possibleMoonMomentData[0].matches(MoonMomentData.HOUR_MINUTE_FORMAT)) {
                    moonMomentDataList.add(
                            new MoonMomentData(
                                    possibleMoonMomentData[0], possibleMoonMomentData[1],
                                    possibleMoonMomentData[2], possibleMoonMomentData[3]
                            )
                    );
                    //Log.d(LOGTAG, line);
                }
            }

            return moonMomentDataList;
        }

        private void saveMoonMoments(LinkedList<MoonMomentData> moonMomentDataList,
                                     DalyMoonRawDataRequest dalyMoonRawDataRequest) {
            for (MoonMomentData moonMomentData : moonMomentDataList) {
                moonMomentDao.insertMoonMoment(
                        new MoonMoment(
                                getDateMillis(dalyMoonRawDataRequest, moonMomentData.getHourMinute()),
                                "", -34.548787f, -58.443638f,
                                Float.parseFloat(moonMomentData.getAltitude()),
                                Float.parseFloat(moonMomentData.getAzimuth()),
                                Float.parseFloat(moonMomentData.getFractionIlluminated())
                        )
                );
            }
        }

        private long getDateMillis(DalyMoonRawDataRequest dalyMoonRawDataRequest, String hourMinutesString) {
            String[] hourMinutesArray = hourMinutesString.split(":");
            return new DateTime(
                    dalyMoonRawDataRequest.getYear(),
                    dalyMoonRawDataRequest.getMonth(),
                    dalyMoonRawDataRequest.getDay(),
                    Integer.parseInt(hourMinutesArray[0]),
                    Integer.parseInt(hourMinutesArray[1]),
                    DateTimeZone.UTC
            ).getMillis();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        @Override
        protected void onPostExecute(Void result) {
            loadedLiveData.setValue(true);
        }
    }

    public void startMoonMomentsLoad() {
        moonMomentDao.nukeTable();
        LoadAllNextMoonMomentsAsyncTask loadAllNextMoonMomentsAsyncTask =
                new LoadAllNextMoonMomentsAsyncTask();
        loadAllNextMoonMomentsAsyncTask.execute();
    }

    private class LoadAllNextMoonMomentsAsyncTask extends AsyncTask<Void, Integer, Void> {

        LoadAllNextMoonMomentsAsyncTask() {
        }

        @Override
        protected Void doInBackground(Void... params) {
            CsvUtils.importFromCsv(moonMomentDao, "moonData1570769284118.csv"); /*moonData1570889553770.csv*/
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        @Override
        protected void onPostExecute(Void result) {
            loadedLiveData.setValue(true);
        }
    }

    public List<MoonMoment> getAllMoonMoments() {
        return moonMomentDao.getAllMoonMoments();
    }

    public void exportDataBaseToCsv() {
        ExportDataBaseToCsvAsyncTask exportDataBaseToCsvAsyncTask = new ExportDataBaseToCsvAsyncTask();
        exportDataBaseToCsvAsyncTask.execute();
    }

    private class ExportDataBaseToCsvAsyncTask extends AsyncTask<Void, Integer, Void> {

        ExportDataBaseToCsvAsyncTask() {
        }

        @Override
        protected Void doInBackground(Void... params) {
            CsvUtils.exportToCsv(moonMomentDao, "moonData" + new DateTime().getMillis() + ".csv");
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        @Override
        protected void onPostExecute(Void result) {
            exportedLiveData.setValue(true);
        }
    }

    public List<MoonMoment> getRestrictedMoonMoments(long date,
                                                     float lowerAzimuth, float higherAzimuth,
                                                     float lowerAltitude, float higherAltitude,
                                                     float minIlluminatedFraction) {
        return moonMomentDao.getMoonMomentsBetweenMargins(
                date,
                lowerAzimuth, higherAzimuth,
                lowerAltitude, higherAltitude,
                minIlluminatedFraction
        );
    }

    public MoonMoment getCurrentMoonMoment() {
        long currentDateMillis = new DateTime(DateTimeZone.UTC).getMillis();
        Log.d(LOGTAG, String.format("Current date: %s (%d)",
                Utils.getFullDateString(new DateTime(currentDateMillis)), currentDateMillis));

        long minDateMillis = currentDateMillis - 600000; //+ (3 * 60 * 60 * 1000);
        Log.d(LOGTAG, "Min date: " + Utils.getFullDateString(new DateTime(minDateMillis)));

        long maxDateMillis = currentDateMillis + 600000; //+ (3 * 60 * 60 * 1000);
        Log.d(LOGTAG, "Max date: " + Utils.getFullDateString(new DateTime(maxDateMillis)));

        return moonMomentDao.getMoonMomentByDate(minDateMillis, maxDateMillis);
    }

    public MoonMoment getNearestMoonMoment() {
        return moonMomentDao.getNearestMoonMoment(new DateTime().getMillis());
    }

    public Integer getMoonMomentsCount() {
        return moonMomentDao.getMoonMomentsCount();
    }
}
