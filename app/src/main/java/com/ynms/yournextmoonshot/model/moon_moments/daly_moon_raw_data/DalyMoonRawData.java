package com.ynms.yournextmoonshot.model.moon_moments.daly_moon_raw_data;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class DalyMoonRawData {

    private final static String LOGTAG = DalyMoonRawData.class.getSimpleName();
    private StringBuilder builder = new StringBuilder();

    public DalyMoonRawData(DalyMoonRawDataRequest dalyMoonRawDataRequest) {
        try {
            Log.d(LOGTAG, dalyMoonRawDataRequest.getUrl());
            Document doc = Jsoup.connect(dalyMoonRawDataRequest.getUrl()).get();
            Elements links = doc.select("pre");

            for (Element link : links) {
                builder.append(link.text());
            }
            Log.d(LOGTAG, builder.toString());
        } catch (IOException e) {
            builder.append("Error : ").append(e.getMessage()).append("n");
        }
    }

    public String[] getLines() {
        return builder.toString().split("\\n");
    }

    public String[] getLineData(String line) {
        return line.split("\\s+");
    }
}
