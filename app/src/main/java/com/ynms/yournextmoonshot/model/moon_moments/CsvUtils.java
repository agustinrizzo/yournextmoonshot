package com.ynms.yournextmoonshot.model.moon_moments;

import android.database.Cursor;
import android.os.Environment;
import android.util.Log;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.ynms.yournextmoonshot.Utils;
import com.ynms.yournextmoonshot.model.moon_moments.moon_moment.MoonMoment;
import com.ynms.yournextmoonshot.model.moon_moments.moon_moment.MoonMomentDao;

import org.joda.time.DateTime;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CsvUtils {

    private static final String LOGTAG = CsvUtils.class.getSimpleName();
    private static final String FILES_PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString();

    public static void exportToCsv(MoonMomentDao moonMomentDao, String fileName) {
        Cursor curCSV = moonMomentDao.getAllMoonMomentCursor();

        File exportDir = new File(FILES_PATH, "");
        if (!exportDir.exists()) {
            exportDir.mkdirs();
        }

        File file = new File(exportDir, fileName);
        try {
            file.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
            csvWrite.writeNext(curCSV.getColumnNames());
            while (curCSV.moveToNext()) {
                String arrStr[] = new String[curCSV.getColumnCount() + 1];
                for (int i = 0; i < curCSV.getColumnCount(); i++) {
                    arrStr[i] = curCSV.getString(i);
                }
                DateTime dateTime = new DateTime(Long.parseLong(curCSV.getString(0)));
                arrStr[curCSV.getColumnCount()] = Utils.getFullDateString(dateTime);
                csvWrite.writeNext(arrStr);
            }
            csvWrite.close();
            curCSV.close();
        } catch (Exception sqlEx) {
            Log.e(LOGTAG, sqlEx.getMessage(), sqlEx);
        }
    }

    public static void importFromCsv(MoonMomentDao moonMomentDao, String fileName) {
        try {
            CSVReader csvReader = new CSVReader(new FileReader(FILES_PATH + "/" + fileName));
            String[] nextLine;
            StringBuilder value = new StringBuilder();

            csvReader.readNext(); // se consume la primera linea del archivo
            int count = 1;
            while ((nextLine = csvReader.readNext()) != null) {
                MoonMoment moonMoment = new MoonMoment(
                        Long.parseLong(nextLine[0]),
                        nextLine[1],
                        Float.parseFloat(nextLine[2]),
                        Float.parseFloat(nextLine[3]),
                        Float.parseFloat(nextLine[4]),
                        Float.parseFloat(nextLine[5]),
                        Float.parseFloat(nextLine[6])
                );
                moonMomentDao.insertMoonMoment(moonMoment);
                count++;
            }
            Log.d(LOGTAG, "lines: " + count);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
