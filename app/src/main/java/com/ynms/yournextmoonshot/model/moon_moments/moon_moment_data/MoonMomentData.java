package com.ynms.yournextmoonshot.model.moon_moments.moon_moment_data;

public class MoonMomentData {

    public static final String HOUR_MINUTE_FORMAT = "[0-9][0-9]:[0-9][0-9]";

    private String hourMinute;
    private String altitude;
    private String azimuth;
    private String fractionIlluminated;

    public MoonMomentData(String hourMinute, String altitude, String azimuth, String fractionIlluminated) {
        this.hourMinute = hourMinute;
        this.altitude = altitude;
        this.azimuth = azimuth;
        this.fractionIlluminated = fractionIlluminated;
    }

    public String getHourMinute() {
        return hourMinute;
    }

    public void setHourMinute(String hourMinute) {
        this.hourMinute = hourMinute;
    }

    public String getAltitude() {
        return altitude;
    }

    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    public String getAzimuth() {
        return azimuth;
    }

    public void setAzimuth(String azimuth) {
        this.azimuth = azimuth;
    }

    public String getFractionIlluminated() {
        return fractionIlluminated;
    }

    public void setFractionIlluminated(String fractionIlluminated) {
        this.fractionIlluminated = fractionIlluminated;
    }
}
