package com.ynms.yournextmoonshot.model.moon_moments.moon_moment;

import android.database.Cursor;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;


@Dao
public interface MoonMomentDao {

    // Devuelve todos los momentos de la luna
    @Query("SELECT * FROM moon_moment")
    List<MoonMoment> getAllMoonMoments();

    @Query("SELECT * FROM moon_moment")
    Cursor getAllMoonMomentCursor();

    @Insert(onConflict = REPLACE)
    void insertMoonMoment(MoonMoment moonMoment);

    @Query("DELETE FROM moon_moment")
    void nukeTable();

    @Query("SELECT count(*) FROM moon_moment")
    Integer getMoonMomentsCount();

    @Query("SELECT * FROM moon_moment WHERE date BETWEEN :lowerDate AND :higherDate")
    List<MoonMoment> getMoonMomentsBetweenDates(float lowerDate, float higherDate);

    // Devuelve la lista de momentos de la luna a partir de una fecha y que están dentro de ciertos
    // límites inferiores y superiores de azimuth y altitud y que cumplen con un porcentaje de
    // luminocidad mínima. La lista se orden desde la fecha mas cercana a la mas lejana
    @Query("SELECT * FROM moon_moment " +
            "WHERE date > :date " +
            "AND azimuth BETWEEN :lowerAzimuth AND :higherAzimuth " +
            "AND altitude BETWEEN :lowerAltitude AND :higherAltitude " +
            "AND fraction_illuminated >= :minIlluminatedFraction " +
            "ORDER BY date ASC")
    List<MoonMoment> getMoonMomentsBetweenMargins(
            long date,
            float lowerAzimuth, float higherAzimuth,
            float lowerAltitude, float higherAltitude,
            float minIlluminatedFraction);

    // Dada una fecha en milisegundos devuelve el momento de la luna más próximo que existe con una
    // diferencia de máximo 10 minutos)
    @Query("SELECT * FROM moon_moment WHERE date BETWEEN :minDate AND :maxDate")
    MoonMoment getMoonMomentByDate(long minDate, long maxDate);

    @Query("SELECT * FROM moon_moment WHERE date >= :date AND altitude > 0")
    MoonMoment getNearestMoonMoment(long date);
}
