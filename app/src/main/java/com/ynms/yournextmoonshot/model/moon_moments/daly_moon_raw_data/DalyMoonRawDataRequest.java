package com.ynms.yournextmoonshot.model.moon_moments.daly_moon_raw_data;

import android.location.Location;

import org.joda.time.DateTime;

public class DalyMoonRawDataRequest {

    //https://aa.usno.navy.mil/cgi-bin/aa_altazw.pl?form=2&body=11&year=2019&month=12&day=23&intv_mag=10&place=Capital+Federal&lon_sign=-1&lon_deg=58&lon_min=26&lat_sign=-1&lat_deg=34&lat_min=32&tz=&tz_sign=-1

    /*
    "https://aa.usno.navy.mil/cgi-bin/aa_altazw.pl?"
    "form=2" & "body=11" & "year=2019" & "month=12" & "day=23" & "intvMag=10" & "place=Capital+Federal"&
    "lonSign=-1" & "lonDeg=58" & "lonMin=26" & "latSign=-1" & "latDeg=34" & "latMin=32" & "tz=" & "tzSign=-1"
    */

    private Integer form; // indica si se usa el form para ubicaciones de EEUU o para ubicaciones internacionales
    private Integer body; // ???

    private Integer year;
    private Integer month;
    private Integer day;

    private Integer intvMag; // intervalo de tiempo entre datos

    private String place; // la ciudad (podemos dejar de lado este parámetro)

    private Integer lonSign;
    private Integer lonDeg;
    private Integer lonMin;

    private Integer latSign;
    private Integer latDeg;
    private Integer latMin;

    private String tz;
    private Integer tzSign;


    public DalyMoonRawDataRequest(DateTime localDate, Location location, int timeIntervalMins) {

        form = 2;
        body = 11;

        year = localDate.getYear();
        month = localDate.getMonthOfYear();
        day = localDate.getDayOfMonth();

        intvMag = timeIntervalMins;

        lonSign = -1;//location.getLongitude() < 0 ? -1 : 1;;
        lonDeg = 58;
        lonMin = 26;

        latSign = -1;//location.getLatitude() < 0 ? -1 : 1;
        latDeg = 34;//Location.convert(location.getLatitude(), location.FORMAT_DEGREES);
        latMin = 32;

        tz = "";
        tzSign = -1;
    }

    public DalyMoonRawDataRequest(Integer form, Integer body, Integer year, Integer month, Integer day,
                                  Integer intvMag, String place, Integer lonSign, Integer lonDeg,
                                  Integer lonMin, Integer latSign, Integer latDeg, Integer latMin,
                                  String tz, Integer tzSign) {
        this.form = form;
        this.body = body;
        this.year = year;
        this.month = month;
        this.day = day;
        this.intvMag = intvMag;
        this.place = place;
        this.lonSign = lonSign;
        this.lonDeg = lonDeg;
        this.lonMin = lonMin;
        this.latSign = latSign;
        this.latDeg = latDeg;
        this.latMin = latMin;
        this.tz = tz;
        this.tzSign = tzSign;
    }

    public String getUrl() {
        return String.format("https://aa.usno.navy.mil/cgi-bin/aa_altazw.pl?form=%d&body=%d&" +
                        "year=%d&month=%d&day=%d&intv_mag=%d&lon_sign=%d&lon_deg=%d&lon_min=%d&" +
                        "lat_sign=%d&lat_deg=%d&lat_min=%d&tz=%s&tz_sign=%d",
                form, body, year, month, day, intvMag, lonSign, lonDeg, lonMin, latSign, latDeg,
                latMin, tz, tzSign);
    }

    public Integer getForm() {
        return form;
    }

    public void setForm(Integer form) {
        this.form = form;
    }

    public Integer getBody() {
        return body;
    }

    public void setBody(Integer body) {
        this.body = body;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getIntvMag() {
        return intvMag;
    }

    public void setIntvMag(Integer intvMag) {
        this.intvMag = intvMag;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Integer getLonSign() {
        return lonSign;
    }

    public void setLonSign(Integer lonSign) {
        this.lonSign = lonSign;
    }

    public Integer getLonDeg() {
        return lonDeg;
    }

    public void setLonDeg(Integer lonDeg) {
        this.lonDeg = lonDeg;
    }

    public Integer getLonMin() {
        return lonMin;
    }

    public void setLonMin(Integer lonMin) {
        this.lonMin = lonMin;
    }

    public Integer getLatSign() {
        return latSign;
    }

    public void setLatSign(Integer latSign) {
        this.latSign = latSign;
    }

    public Integer getLatDeg() {
        return latDeg;
    }

    public void setLatDeg(Integer latDeg) {
        this.latDeg = latDeg;
    }

    public Integer getLatMin() {
        return latMin;
    }

    public void setLatMin(Integer latMin) {
        this.latMin = latMin;
    }

    public String getTz() {
        return tz;
    }

    public void setTz(String tz) {
        this.tz = tz;
    }

    public Integer getTzSign() {
        return tzSign;
    }

    public void setTzSign(Integer tzSign) {
        this.tzSign = tzSign;
    }
}
