package com.ynms.yournextmoonshot.model.moon_moments.moon_moment;

import androidx.room.ColumnInfo;
import androidx.room.Entity;


@Entity(tableName = "moon_moment", primaryKeys = {"date", "latitude", "longitude"})
public class MoonMoment {

    /*
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    */

    @ColumnInfo(name = "date")
    private long date;

    @ColumnInfo(name = "place")
    private String place;

    @ColumnInfo(name = "latitude")
    private float latitude;

    @ColumnInfo(name = "longitude")
    private float longitude;

    @ColumnInfo(name = "altitude")
    private float altitude;

    @ColumnInfo(name = "azimuth")
    private float azimuth;

    @ColumnInfo(name = "fraction_illuminated")
    private float fractionIlluminated;

    public MoonMoment() {
    }

    public MoonMoment(long date,
                      String place, float latitude, float longitude,
                      float altitude, float azimuth, float fractionIlluminated) {
        this.date = date;
        this.place = place;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.azimuth = azimuth;
        this.fractionIlluminated = fractionIlluminated;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getAltitude() {
        return altitude;
    }

    public void setAltitude(float altitude) {
        this.altitude = altitude;
    }

    public float getAzimuth() {
        return azimuth;
    }

    public void setAzimuth(float azimuth) {
        this.azimuth = azimuth;
    }

    public float getFractionIlluminated() {
        return fractionIlluminated;
    }

    public void setFractionIlluminated(float fractionIlluminated) {
        this.fractionIlluminated = fractionIlluminated;
    }
}
