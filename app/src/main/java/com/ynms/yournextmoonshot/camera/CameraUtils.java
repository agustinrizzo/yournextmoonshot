package com.ynms.yournextmoonshot.camera;

import android.content.Context;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.util.SizeF;

import static android.content.Context.CAMERA_SERVICE;

public class CameraUtils {

    public static Fov calculateFOV(Context context) {
        try {
            CameraManager cameraManager = (CameraManager) context.getSystemService(CAMERA_SERVICE);
            for (final String cameraId : cameraManager.getCameraIdList()) {
                CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraId);
                int cOrientation = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (cOrientation == CameraCharacteristics.LENS_FACING_BACK) {
                    float[] maxFocus = characteristics.get(CameraCharacteristics.LENS_INFO_AVAILABLE_FOCAL_LENGTHS);
                    SizeF size = characteristics.get(CameraCharacteristics.SENSOR_INFO_PHYSICAL_SIZE);

                    float w = size.getWidth();
                    float h = size.getHeight();

                    float horizonalAngle = (float) (2 * Math.atan(w / (maxFocus[0] * 2)));
                    float verticalAngle = (float) (2 * Math.atan(h / (maxFocus[0] * 2)));

                    return new Fov(
                            (float) Math.toDegrees(horizonalAngle),
                            (float) Math.toDegrees(verticalAngle)
                    );
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
