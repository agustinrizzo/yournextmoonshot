package com.ynms.yournextmoonshot.camera;

import java.io.Serializable;

public class Fov implements Serializable {

    private float horizonalAngle;
    private float verticalAngle;

    public Fov(float horizonalAngle, float verticalAngle) {
        this.horizonalAngle = horizonalAngle;
        this.verticalAngle = verticalAngle;
    }

    public float getHorizonalAngle() {
        return horizonalAngle;
    }

    public void setHorizonalAngle(float horizonalAngle) {
        this.horizonalAngle = horizonalAngle;
    }

    public float getVerticalAngle() {
        return verticalAngle;
    }

    public void setVerticalAngle(float verticalAngle) {
        this.verticalAngle = verticalAngle;
    }
}
