package com.ynms.yournextmoonshot.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.SignInButton;
import com.ynms.yournextmoonshot.R;
import com.ynms.yournextmoonshot.main.view.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;


public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.textViewTitle)
    TextView textViewTitle;

    @BindView(R.id.signInButton)
    SignInButton signInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        ButterKnife.bind(this);

        initGui();
    }

    private void initGui() {
        getSupportActionBar().hide();
        textViewTitle.setText(Html.fromHtml("Your Next <b>Moonshot</b>"));
        initSignInButton();
    }

    private void initSignInButton() {
        signInButton.setColorScheme(SignInButton.COLOR_DARK);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        TextView textView = (TextView) signInButton.getChildAt(0);
        textView.setText("Iniciar sesión con Google");
        signInButton.setOnClickListener(v -> {
            Intent openMainActivityIntent = new Intent(this, MainActivity.class);
            startActivity(openMainActivityIntent);
            finish();
        });
    }

}
