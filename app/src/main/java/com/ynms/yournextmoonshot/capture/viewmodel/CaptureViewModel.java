package com.ynms.yournextmoonshot.capture.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.ynms.yournextmoonshot.sensors.Orientation;
import com.ynms.yournextmoonshot.sensors.OrientationRepository;
import com.ynms.yournextmoonshot.viewmodel.MoonshotViewModel;

public class CaptureViewModel extends AndroidViewModel {

    private static final String LOGTAG = MoonshotViewModel.class.getSimpleName();

    private OrientationRepository orientationRepository;

    private String capturePath;
    public String getCapturePath() {
        return capturePath;
    }
    public void setCapturePath(String capturePath) {
        this.capturePath = capturePath;
    }

    private LiveData<Orientation> orientationLiveData;
    public LiveData<Orientation> getOrientationLiveData() {
        return orientationLiveData;
    }

    public CaptureViewModel(@NonNull Application application) {
        super(application);

        orientationRepository = new OrientationRepository(getApplication());
        orientationLiveData = orientationRepository.getOrientationLiveData();
    }

    public void resumeOrientation() {
        orientationRepository.resume();
    }

    public void pauseOrientation() {
        orientationRepository.pause();
    }

}
