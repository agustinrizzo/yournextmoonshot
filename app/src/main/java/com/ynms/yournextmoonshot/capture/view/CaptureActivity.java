/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ynms.yournextmoonshot.capture.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.ynms.yournextmoonshot.R;
import com.ynms.yournextmoonshot.camera.Fov;
import com.ynms.yournextmoonshot.capture.viewmodel.CaptureViewModel;
import com.ynms.yournextmoonshot.main.model.Moonshot;
import com.ynms.yournextmoonshot.moonshot.MoonshotSetPropertiesActivity;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CaptureActivity extends AppCompatActivity {

    private static final String LOGTAG = CaptureActivity.class.getSimpleName();

    @BindView(R.id.textViewHeading)
    TextView textViewHeading;
    @BindView(R.id.imageViewHeading)
    ImageView imageViewHeading;

    @BindView(R.id.textViewElevation)
    TextView textViewElevation;
    @BindView(R.id.imageViewElevation)
    ImageView imageViewElevation;

    private CaptureViewModel captureViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.capture_activity);
        ButterKnife.bind(this);

        getSupportActionBar().hide();

        initGui(savedInstanceState);
        initViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        captureViewModel.resumeOrientation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        captureViewModel.pauseOrientation();
    }

    private void initGui(Bundle savedInstanceState) {
        if (null == savedInstanceState) {
            CameraFragment cameraFragment = new CameraFragment();
            cameraFragment.setOnPhotoTakenListener(new CameraFragment.OnPhotoTakenListener() {
                @Override
                public void stopOrientation() {
                    captureViewModel.pauseOrientation();
                }

                @Override
                public void onPhotoTaken(@NotNull String capturePath) {
                    captureViewModel.setCapturePath(capturePath);
                    /*
                    hide();

                    MoonshotSetPropertiesFragment moonshotSetPropertiesFragment =
                            new MoonshotSetPropertiesFragment();
                    moonshotSetPropertiesFragment.setCapture(captureFileName);

                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, moonshotSetPropertiesFragment)
                            .commit();
                    */

                    Intent openActivityIntent = new Intent(
                            CaptureActivity.this, MoonshotSetPropertiesActivity.class);
                    openActivityIntent.putExtra("capturePath", capturePath);
                    startActivityForResult(openActivityIntent, 2);

                    /*
                    Intent intent = new Intent();
                    intent.putExtra("captureFileName", captureFileName);
                    intent.putExtra("orientation", orientation);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                    */
                }
            });

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, cameraFragment)
                    .commit();
        }
    }

    /*
    private void hide() {
        textViewHeading.setVisibility(View.INVISIBLE);
        imageViewHeading.setVisibility(View.INVISIBLE);
        textViewElevation.setVisibility(View.INVISIBLE);
        imageViewElevation.setVisibility(View.INVISIBLE);
    }
    */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    Moonshot moonshot = new Moonshot(
                            data.getStringExtra("name"),
                            0.0f, 0.0f, captureViewModel.getOrientationLiveData().getValue(),
                            data.getFloatExtra("minMoonIllumination", 1.0f),
                            data.getFloatExtra("minSkyDarkness", 1.0f),
                            data.getIntExtra("lens", 1),
                            captureViewModel.getCapturePath(),
                            new DateTime(),
                            new Fov(1.0f, 1.0f)
                    );
                    moonshot.setTargetDate(new DateTime().plusDays(4).plusHours(7).plusMinutes(4));

                    Intent intent = new Intent();
                    intent.putExtra("moonshot", moonshot);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            } else if (resultCode == 0) {
                Log.d(LOGTAG, "RESULT CANCELLED");
            }
        }
    }

    private void initViewModel() {
        captureViewModel = ViewModelProviders.of(this).get(CaptureViewModel.class);

        captureViewModel.getOrientationLiveData().observe(this, orientation -> {
            textViewHeading.setText(String.format("%d°", (int) orientation.getAzimuth()));
            textViewElevation.setText(String.format("%d°", (int) orientation.getElevation()));
        });
    }
}
