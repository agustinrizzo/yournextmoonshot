package com.ynms.yournextmoonshot.communications.model;

/*
"main": {
        "temp": 16.46,
        "temp_min": 16.46,
        "temp_max": 17.18,
        "pressure": 1016.84,
        "sea_level": 1016.84,
        "grnd_level": 977.42,
        "humidity": 91,
        "temp_kf": -0.72
    },
 */

import com.google.gson.annotations.SerializedName;

public class Main {

    @SerializedName("temp")
    private float temp;

    @SerializedName("temp_min")
    private float tempMin;

    @SerializedName("temp_max")
    private float tempMax;

    @SerializedName("pressure")
    private float pressure;

    @SerializedName("sea_level")
    private float seaLevel;

    @SerializedName("grnd_level")
    private float grnd_level;

    @SerializedName("humidity")
    private float humidity;

    @SerializedName("temp_kf")
    private float tempKf;

    public Main() {
    }

    public Main(float temp, float tempMin, float tempMax, float pressure, float seaLevel,
                float grnd_level, float humidity, float tempKf) {
        this.temp = temp;
        this.tempMin = tempMin;
        this.tempMax = tempMax;
        this.pressure = pressure;
        this.seaLevel = seaLevel;
        this.grnd_level = grnd_level;
        this.humidity = humidity;
        this.tempKf = tempKf;
    }

    public float getTemp() {
        return temp;
    }

    public void setTemp(float temp) {
        this.temp = temp;
    }

    public float getTempMin() {
        return tempMin;
    }

    public void setTempMin(float tempMin) {
        this.tempMin = tempMin;
    }

    public float getTempMax() {
        return tempMax;
    }

    public void setTempMax(float tempMax) {
        this.tempMax = tempMax;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public float getSeaLevel() {
        return seaLevel;
    }

    public void setSeaLevel(float seaLevel) {
        this.seaLevel = seaLevel;
    }

    public float getGrnd_level() {
        return grnd_level;
    }

    public void setGrnd_level(float grnd_level) {
        this.grnd_level = grnd_level;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public float getTempKf() {
        return tempKf;
    }

    public void setTempKf(float tempKf) {
        this.tempKf = tempKf;
    }
}
