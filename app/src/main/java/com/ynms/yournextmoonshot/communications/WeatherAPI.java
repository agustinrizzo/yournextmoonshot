package com.ynms.yournextmoonshot.communications;

import com.ynms.yournextmoonshot.communications.model.WeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface WeatherAPI {

    @GET("forecast/")
    Call<WeatherResponse> getWeatherData(
            @Query("lat") float lat,
            @Query("lon") float lon,
            @Query("units") String units,
            @Query("appid") String appid
    );
}