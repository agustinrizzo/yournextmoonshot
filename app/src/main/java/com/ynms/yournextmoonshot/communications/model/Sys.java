package com.ynms.yournextmoonshot.communications.model;

/*
"sys": {
        "pod": "n"
    },
 */

import com.google.gson.annotations.SerializedName;

public class Sys {

    @SerializedName("pod")
    private String pod;

    public Sys() {
    }

    public Sys(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }
}
