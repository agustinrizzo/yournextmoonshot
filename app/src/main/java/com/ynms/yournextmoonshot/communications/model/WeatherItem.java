package com.ynms.yournextmoonshot.communications.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/*
    "dt": 1571065200,
    "main": {
        "temp": 16.46,
        "temp_min": 16.46,
        "temp_max": 17.18,
        "pressure": 1016.84,
        "sea_level": 1016.84,
        "grnd_level": 977.42,
        "humidity": 91,
        "temp_kf": -0.72
    },
    "weather": [
        {
            "id": 804,
            "main": "Clouds",
            "description": "overcast clouds",
            "icon": "04n"
        }
    ],
    "clouds": {
        "all": 100
    },
    "wind": {
        "speed": 0.95,
        "deg": 140.218
    },
    "sys": {
        "pod": "n"
    },
    "dt_txt": "2019-10-14 15:00:00"
 */

public class WeatherItem {

    @SerializedName("dt")
    private long dt;

    @SerializedName("main")
    private Main main;

    @SerializedName("weather")
    private List<Weather> weather;

    @SerializedName("clouds")
    private Clouds clouds;

    @SerializedName("sys")
    private Sys sys;

    @SerializedName("dt_txt")
    private String dtTxt;

    public WeatherItem() {
    }

    public WeatherItem(long dt, Main main, List<Weather> weather, Clouds clouds, Sys sys, String dtTxt) {
        this.dt = dt;
        this.main = main;
        this.weather = weather;
        this.clouds = clouds;
        this.sys = sys;
        this.dtTxt = dtTxt;
    }

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public String getDtTxt() {
        return dtTxt;
    }

    public void setDtTxt(String dtTxt) {
        this.dtTxt = dtTxt;
    }
}
