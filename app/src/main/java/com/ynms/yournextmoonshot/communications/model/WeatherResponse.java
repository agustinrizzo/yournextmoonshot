package com.ynms.yournextmoonshot.communications.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/*
{
    "cod": "200",
    "message": 0.0079,
    "cnt": 40,
    "list": [
    ]
 */

public class WeatherResponse {

    @SerializedName("cod")
    private int cod;

    @SerializedName("message")
    private float message;

    @SerializedName("cnt")
    private int cnt;

    @SerializedName("list")
    private List<WeatherItem> list;

    public WeatherResponse() {
    }

    public WeatherResponse(int cod, float message, int cnt, List<WeatherItem> list) {
        this.cod = cod;
        this.message = message;
        this.cnt = cnt;
        this.list = list;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public float getMessage() {
        return message;
    }

    public void setMessage(float message) {
        this.message = message;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public List<WeatherItem> getList() {
        return list;
    }

    public void setList(List<WeatherItem> list) {
        this.list = list;
    }
}
